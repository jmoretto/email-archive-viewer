import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './emails/list/list.component';
import { ItemComponent } from './emails/item/item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HighlightPipe } from './pipes/highlight.pipe';
import { EditableDivComponent } from './common/editable-div/editable-div.component';
import { HeaderComponent } from './layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ItemComponent,
    HighlightPipe,
    EditableDivComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
