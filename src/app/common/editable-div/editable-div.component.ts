import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-editable-div',
  template: `
    <div [innerHTML]="htmlContent"></div>
  `,
})
export class EditableDivComponent implements OnInit {
  @Input() content: string = "";
  @Input() prefix: string = "";

  // Properties
  public htmlContent: string = "";

  constructor() { }

  ngOnInit(): void {
    this.htmlContent = `${this.prefix}${this.content}`;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.htmlContent = `${changes.prefix ? changes.prefix.currentValue : this.prefix}${changes.content ? changes.content.currentValue : this.content}`;
  }
}
