import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableDivComponent } from './editable-div.component';

describe('EditableDivComponent', () => {
  let component: EditableDivComponent;
  let fixture: ComponentFixture<EditableDivComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditableDivComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
