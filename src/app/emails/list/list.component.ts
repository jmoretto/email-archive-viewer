import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import Email from '../../models/email.model';
import { faChevronRight, faChevronLeft, faInbox } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-email-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() emails: Email[] = [];
  @Input() selectedEmail: Email | null = null;
  @Input() filterText: string = '';
  @Output() emailSelected: EventEmitter<Email> = new EventEmitter();
  @Output() filterChanged: EventEmitter<string> = new EventEmitter();

  // Properties
  limit: number = 10;
  offset: number = 0;
  shownEmails: Email[] = [];
  filteredEmails: Email[] = [];
  numberOfPages: number = 1;
  currentPage: number = 1;
  filterInput: string = '';

  // Icons
  faChevronRight = faChevronRight;
  faChevronLeft = faChevronLeft;
  faInbox = faInbox;

  constructor() {}

  ngOnInit(): void {
    this.filteredEmails = this.emails.slice(0);
    this.shownEmails = this.filteredEmails.slice(0, 10);
    this.numberOfPages = Math.ceil(this.filteredEmails.length / 10) || 1;
  }

  selectEmail(email: Email): void {
    this.emailSelected.emit(email);
  }

  updateShownEmails() {
    this.shownEmails = this.filteredEmails.slice(this.offset, this.offset + this.limit);
    this.numberOfPages = Math.ceil(this.filteredEmails.length / 10) || 1;
  }

  changePage(goToNextPage: boolean): void {
    if (goToNextPage) {
      if (this.currentPage !== this.numberOfPages) {
        this.currentPage++;
        this.offset += 10;
        this.updateShownEmails()
      }
    } else {
      if (this.currentPage !== 1) {
        this.currentPage--;
        this.offset -= 10;
        this.updateShownEmails()
      }
    }
  }

  handleFilter(): void {
    if (this.filterInput !== '') {
      // Filter the emails by the text.
      this.filteredEmails = this.emails.filter(
        (email) => JSON.stringify(email)
                       .toLowerCase()
                       .indexOf(this.filterInput.toLowerCase()) !== -1);
    } else {
      // No filter, just return all emails.
      this.filteredEmails = this.emails.slice(0);
    }
    this.currentPage = 1;
    this.offset = 0;
    this.updateShownEmails();
    this.filterChanged.emit(this.filterInput);
  }
}
