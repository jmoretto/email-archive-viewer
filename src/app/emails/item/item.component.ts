import { Component, Input, OnInit, ChangeDetectionStrategy, SimpleChanges } from '@angular/core';
import Email from 'src/app/models/email.model';
import { faCalendarAlt, faUser, faUsers, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-email-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit {
  @Input() email: Email | null = null;
  @Input() filterText: string = '';

  public filter: string = '';

  // Icons
  faCalendarAlt = faCalendarAlt;
  faUser = faUser;
  faUsers = faUsers;
  faArrowCircleLeft = faArrowCircleLeft;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filterText) {
      this.filter = changes.filterText.currentValue;
    }
  }
}
