/**
 * Email class model
 */
export default class Email {
  constructor(
    public id: string,
    public sender: string,
    public c_receivers: string[],
    public subject: string,
    public body: string,
    public date: string, // Should be an ISO string,
  ) {};
}