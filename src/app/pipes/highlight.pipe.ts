import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlight'
})
export class HighlightPipe implements PipeTransform {
  transform(value: any, filterText: string): string {
    const re = new RegExp(`(${filterText})`, 'gi');

    if (filterText !== '' && typeof value === 'string') {
      return value.replace(re, '<mark>$1</mark>');
    } else if (filterText !== '' && Array.isArray(value)) {
      return value.map(currentValue => currentValue.replace(re, '<mark>$1</mark>')).join(',');
    }

    return value;
  }
}
