import { Component } from '@angular/core';
import Email from './models/email.model';
import { ArchiveService } from './services/archive.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  emailList: Email[] = [];
  selectedEmail: Email | null = null;
  filterText: string = '';

  constructor(private archiveService: ArchiveService) {
    // Get the initial email list.
    this.emailList = this.archiveService.getEmails();
  }

  selectEmail(email: Email) {
    this.selectedEmail = email;
  }

  changeFilter(text: string) {
    this.filterText = text;
  }
}
