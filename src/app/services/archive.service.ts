import { Injectable } from '@angular/core';
import db from '../db/db.json';
import Email from '../models/email.model';

@Injectable({
  providedIn: 'root'
})
export class ArchiveService {
  archive: Email[] = db;

  constructor() { }

  getEmails(): Email[] {
    return this.archive;
  }
}
