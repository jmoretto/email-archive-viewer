module.exports = {
  prefix: '',
  purge: {
    content: [
      './src/**/*.{html,ts}',
    ]
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      gridTemplateColumns: {
        // Complex site-specific column configuration
        'email-layout': 'minmax(auto, 400px) 4fr',
      },
      gridTemplateRows: {
        'main-layout': '60px 1fr',
      }
    }
  },
  variants: {
    extend: {
      margin: ['last'],
      cursor: ['disabled'],
      backgroundColor: ['disabled', 'active'],
      textColor: ['disabled']
    },
  },
  plugins: [],
};