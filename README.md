# EmailViewer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.11.
It uses SCSS for styling along with TailwindCSS.
Added fontawesome for icons.

# Installing and running

Install angular and the necessary modules.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Technical Details
1. There is no backend. There is only a service called archive which reads from the db.json file which contains the email data.
2. The code was divided between services, components, models and pipes.
3. Currently on the Email model exists. Check email.model.ts for details.
4. There is just one page where the functionality exists:
  a. You can view the list of emails on the left.
  b. You can search by typing in the input field on the left. It should both search the emails and the text within and highlights the text matched.
  c. You can view the details for the email on the right when an email is selected.
5. Highlighting is done through the highlight pipe which uses the <mark> element.


# Tech debt/Enhancemnts
1. Update the service code so that it can communicate with an API passing in the pagination details along with the filter text.
2. No unit test or e2e tests were done.
3. We could probably make the whole email list and view into a separate module which receives the list from outside.
4. There is a lot of repeated CSS, we could try to use the @apply function to make it less repetitive in the HTML.
